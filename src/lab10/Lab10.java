 /*1. Implement a simple node class (structure) for storing numbers and use it for creation of binary tree.
  	  Create a binary tree in main program.
	  a. Method indicating whether a node is internal or external
	  b. The tree for demonstration should have height at least 2
	  c. There should be 2 trees demonstrated, one full and one skewed
   2. For this binary tree create methods, for calculating its:
	  a. Height of the tree
	  b. Sum of the tree elements
	  c. Number of internal and external nodes*/

package lab10;

/* Class containing left and right child of current 
node and its value*/
class Node {
	int value;
	Node left, right;

	public Node(int number) {
		value = number;
		left = right = null;
	}
}

// A Java program to introduce Binary Tree
class BinaryTree {
	// Root of Binary Tree
	Node root;
	
	// Constructors
	BinaryTree(int value) {
		root = new Node(value);
	}
	BinaryTree() {
		root = null;
	}
	
	//1a: Method indicating whether a node is internal or external
	void nodeIndic(Node node) {
		if (node == null) 
			System.out.println("This node is empty.");
		else if (node.left == null && node.right == null)
			System.out.println("This node is external.");
		else
			System.out.println("This node is internal");			
	}

	//2a: Methods calculating height of the tree
	int getHeight(Node node) {
		if (node == null)
			return 0;
		else {
			/* compute the height of each subtree */
			int lHeight = getHeight(node.left);
			int rHeight = getHeight(node.right);

			/* use the larger one */
			if (lHeight > rHeight)
				return (lHeight + 1);
			return (rHeight + 1);
		}
	}
	
	//2b: Methods calculating sum of tree elements
	int getSum(Node root) {
		if (root == null)
			return 0;
		return (root.value + getSum(root.left) + getSum(root.right));
	}

	//2a: Methods calculating number of internal and external nodes
	int countExternal(Node node) {
		if (node == null)
			return 0;
		if (node.left == null && node.right == null)
			return 1;
		return countExternal(node.left) + countExternal(node.right);
	}

	int countInternal(Node root) {
		if (root == null || (root.left == null && root.right == null))
			return 0;
		return 1 + countInternal(root.left) + countInternal(root.right);
	}
}

public class Lab10 {

	public static void main(String[] args) {
		//1b+1c: create a full tree
		BinaryTree tree1 = new BinaryTree();
		tree1.root = new Node(9);					//9
		tree1.root.left = new Node(4);				//4	3
		tree1.root.right = new Node(3);		
		tree1.root.left.left = new Node(7);			//7	6	5	2
		tree1.root.left.right = new Node(6);
		tree1.root.right.left = new Node(5);
		tree1.root.right.right = new Node(2);
		tree1.root.left.left.left = new Node (1);	//1
		
		//1b+1c: create a skewed tree
		BinaryTree tree2 = new BinaryTree();
		tree2.root = new Node(7);					//7
		tree2.root.left = new Node(1);				//1
		tree2.root.left.left = new Node(5);			//5
		tree2.root.left.left.left = new Node(2);	//2

		//demonstrations of methods
		tree1.nodeIndic(tree1.root.left);
		tree1.nodeIndic(tree1.root.left.left.right);
		tree2.nodeIndic(tree2.root.left.left.left);		
		System.out.println("Height of tree1: " + tree1.getHeight(tree1.root));
		System.out.println("Height of tree2: " + tree2.getHeight(tree2.root));
		System.out.println("Sum of tree1 elements: " + tree1.getSum(tree1.root));
		System.out.println("Sum of tree2 elements: " + tree2.getSum(tree2.root));
		System.out.println("Number of internal nodes in tree1: " + tree1.countInternal(tree1.root));
		System.out.println("Number of external nodes in tree1: " + tree1.countExternal(tree1.root));
		System.out.println("Number of internal nodes in tree2: " + tree2.countInternal(tree2.root));
		System.out.println("Number of external nodes in tree2: " + tree2.countExternal(tree2.root));
	}
}
